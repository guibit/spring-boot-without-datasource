package com.gui.semdatabase.resolucao.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gui.semdatabase.resolucao.Model.Lista;
import java.util.List;

@Repository
public interface ListaRepository extends JpaRepository <Lista, Integer> {
    List<Lista> findByNumero (String numero);
    
}