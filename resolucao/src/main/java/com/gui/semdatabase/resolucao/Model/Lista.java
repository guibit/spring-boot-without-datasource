package com.gui.semdatabase.resolucao.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Lista {
    @Column (name="numero")
    @Id
    private Integer numero[];

    public Integer[] getNumero() {
        return numero;
    }

    public void setNumero(Integer[] numero) {
        this.numero = numero;
    }
    
}