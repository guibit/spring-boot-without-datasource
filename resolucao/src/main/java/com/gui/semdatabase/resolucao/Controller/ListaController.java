package com.gui.semdatabase.resolucao.Controller;


import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;




@RestController
@RequestMapping("/")
public class ListaController {
    
    @GetMapping("/listaReversa")
    @ResponseBody
    public String Inverso (@RequestParam List<String> lista) {
        List<String> original = lista ;
        Collections.reverse(original);

        return "Lista Reversa:" + original ;
    }

    @GetMapping("/imprimirImpare")
    @ResponseBody
    public List<Integer> imprimindoImpares (@RequestParam List<Integer> lista){
        //List<Integer> original = lista ;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) % 2 == 0) {
                lista.remove(i);
            }
        }

        return lista;    
    } 


    @GetMapping("/imprimirPares")
    @ResponseBody
    public List<Integer> imprimindoPares (@RequestParam List<Integer> lista){
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) % 2 != 0) {
                lista.remove(i);
            }
        }

        return lista;    
    } 


    @GetMapping("/tamanho")
    @ResponseBody
    public String palavraTamanho (@RequestParam String palavra){
        Integer tamanho = palavra.length(); 
        String tam = String.valueOf(tamanho);

        return "Tamanho="+tam;    
    } 


    @GetMapping("/maiusculas")
    @ResponseBody
    public List<String> transformaMaisc (@RequestParam List<String> palavra){
        palavra.replaceAll(String::toUpperCase);

        return palavra;    
    } 

    @GetMapping("/vogais")
    @ResponseBody
    public char[] vogais (@RequestParam List<String> palavra){
        String tudo = palavra.toString();
        char[] vogais = {'a','e','i','o','u'};
        char[] pVogais = new char [vogais.length];
        int count = 0 ;
        char[] output = new char[50];

        for(int i=0 ; i<tudo.length() ; i++ ){
            for (int j= 0 ; j< vogais.length ; j++) {
                if (Character.toLowerCase(tudo.charAt(i)) == vogais[j] && count <tudo.length()) {
                    pVogais[count ] = tudo.charAt(i);
                    if(count ==0){
                        output[count] = pVogais[count];

                    }
                    count++;
                }
            }
        }

        return output;  
    }
    
    
    @GetMapping("/consoantes")
    @ResponseBody
    public char[] consoante (@RequestParam List<String> palavra){
        palavra.replaceAll(String::toUpperCase);
        String tudo = palavra.toString();
        char[] vogais = {'a','e','i','o','u'};
        char[] pConsoante = new char [vogais.length];
        int count = 0 ;
        char[] output = new char[50];
        for(int i =0; i<tudo.length() ; i++ ){
            switch (tudo.charAt(i)) {
                case 'a':
                    
                    break;
                case 'e':
                    
                    break;
                case 'i':
                    
                    break;  
                case 'o':
                    
                    break;
                case 'u':
                    
                    break;
            
            
                default:
                pConsoante[count] = tudo.charAt(i);
                output[count] = pConsoante[count];
                count++;
                    break;
            }
        }

        return output;    
    } 

    @GetMapping("/nomeBibliografico")
    @ResponseBody
    public String nomeBibliografico (@RequestParam String nome){
        int index= nome.lastIndexOf(" ");
        String restoSentenca = (nome.substring(0, index));
        String ultimaPalavra = (nome.substring(index+1));
        String ultima = ultimaPalavra.toUpperCase();
        String nova = ultima + ", " + restoSentenca;
        return nova;    
    } 

    @GetMapping("/sistemaMonetario")
    @ResponseBody
    public String updateFoos(@RequestParam Integer a , Integer b){
        String output = "Saque R$:" + (a+b) + ": 1 nota de R$:" + a +" e 1 nota de R$"+b ;
        String output2 = "Saque R$:" + (2*a+b)+": 2 notas de R$"+a+ "e 1 nota de R$:"+b;
        String output3 = "Saque R$:" + (a+2*b)+": 1 nota de R$:" +a+"e 2 notas de R$:+"+b;
        String output4 = "Saque R$:" + (6*b) + ": 6 notas de R$:"+b;
        String output5 = "Saque R$:" + (10*b) + ": 10 notas de R$:"+b;
        String output6 = "Saque R$:" + ((2*a)+(10*b)) + ": 2 notas de R$:"+a+" e 10 notas de R$:"+b;
        /*
        Saque R$30: 6 notas de R$5
        Saque R$50: 10 notas de R$5
        Saque R$56: 2 notas de R$3 e 10 notas de R$5*/
        

        return output+"\n\n"+output2+"\n\n"+output3+"\n\n"+output4+"\n\n"+output5+"\n\n"+output6;
    }
    
}

